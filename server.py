from flask import Flask, render_template
from flask import request
import TreeDecomp as td
application = Flask(__name__)

@application.route("/")
def hello_world():
    return "Hello world!"

@application.route("/pythonscript", methods=['GET', 'POST'])
def runScript():
    error = None
    if request.method == 'POST':
        thestr=request.form['data']
        A=td.adj_mat(thestr)
        ls,vec,mul=td.treeDecomp(A)
        newstr=td.strbuild(A,ls,vec,mul)
        return newstr

    return 'error'


if __name__ == '__main__':
    application.run(host='0.0.0.0')
