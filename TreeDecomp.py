
# coding: utf-8

# In[2]:

import numpy as np
from numpy import linalg as la
import re


# In[3]:

def adj_mat(thestr):
    """ Parse the data stored in 'datafile' and form the
    adjacencem matrix of the corresponding graph.
    'n' is the number of rows of the nxn sparse matrix formed. """
    result = [e for e in re.split("[^0-9]", thestr) if e != '']
    n=max(map(int, result))+1
    thelist=thestr.replace(',','\n').split('\n')
    W=np.zeros((n,n))
    for L in thelist:
        words=L.strip().split()
        try:
            m=int(words[0])
            z=int(words[1])
            if m<n and z<n:
                W[m,z]=1
        except:
            pass
    return ((W+W.T)>.5)*1


# In[4]:

def treeDecomp(A):
    def property3(listsets): #O(n^2)
        n=len(listsets)
        where={x:-1 for x in xrange(n)}
        check=set()
        uncheck=set()
        for i in xrange(n):
            curset=listsets[i][0]
            add=check.intersection(curset)
            for q in add:
                for j in xrange(where[q]+1,i):
                    listsets[j][0].add(q)
            check=check.union(uncheck.difference(curset))
            uncheck=set(curset)
            for z in curset:
                where[z]=i
    def rmsubs(listsets):#O(n)
        n=len(listsets)
        i=1
        while i < n:
            if len(listsets[i-1][0].difference(listsets[i][0]))==0:
                listsets.remove(listsets[i-1])
                i-=1
                n-=1
            i+=1
        i=n-1
        while i>0:
            if len(listsets[i][0].difference(listsets[i-1][0]))==0:
                listsets.remove(listsets[i])   
            i-=1
    
    def Fiedler(A): #O(n)
        tol=10**(-6)
        L=np.diag(A.sum(1))-A
        eig,vec=la.eigh(L)
        vals=np.argsort(eig)[1:3]
        mul=0
        if abs(eig[vals[1]]-eig[vals[0]])<tol or eig[vals[0]]<tol:
            mul=1
        return vec[:,vals[0]],mul
    def getNeighbors(A,index):
        return set(np.flatnonzero(A[index]))
    tol=1e-3
    done=set()
    #included=set()
    pos=[]
    neg=[]
    vec,mul=Fiedler(A)
    order=np.argsort(abs(vec))[::-1]
    n = len(order)
    vec*=np.sqrt(n)
    i=0
    B=A.copy()
    while np.sum(B)>0:
        curset={order[i]}
        tempdone={order[i]}
        curset=curset.union(getNeighbors(A,order[i]))
        j=i+1
        while j<n and abs(vec[order[j]]-vec[order[i]])<tol:
            tempdone.add(order[j])
            curset.add(order[j])
            curset=curset.union(getNeighbors(A,order[j]))
            j+=1
        left=curset.difference(done)
        if vec[order[i]]>=0:
            pos.insert(0,(left,order[i:j],vec[order[i]]))
        else:
            neg.append((left,order[i:j],vec[order[i]]))
        done=done.union(tempdone)
        B[np.ix_(list(curset),list(curset))]=0
        #included=included.union(left)
        i=j
    listsets=neg+pos
    property3(listsets)
    rmsubs(listsets)
    return listsets,vec,mul


# In[5]:

def strbuild(A,listsets,vec,mul):
    def getNeighbors(A,index):
        return list(np.flatnonzero(A[index]))
    n = len(A)
    sets={x:[] for x in xrange(n)}
    for i,w in enumerate(listsets):
        for q in w[0]:
            sets[q]+=[i]
    f=[]
    f.append('{\n')
    f.append('\t"multipleEig":'+str(mul)+',\n')
    f.append('\t"nodes":[\n')
    for i in xrange(n):
        f.append('\t\t{"name":"'+str(i)+'","group":'+str(sets[i])+',"value":'+("%.2f" % vec[i])+'}')
        if i!=n-1:
            f.append(',')
        f.append('\n')
    f.append('\t],\n')
    f.append('\t"links":[\n')
    for i in xrange(n):
        t=getNeighbors(A,i)
        for j in t:
            if i>j:
                f.append('\t\t{"source":'+str(i)+',"target":'+str(j)+'}')
                if i!=n-1 or j!=t[-1]:
                    f.append(',')
                f.append('\n')
    f.append('\t],\n')
    f.append('\t"groups":[\n')
    m=len(listsets)
    for i,w in enumerate(listsets):
        f.append('\t\t{"name":"W'+str(i)+'","group":'+str(list(w[0]))+',"main":'+str(list(w[1]))+',"value":'+("%.2f" % w[2])+'}')
        if i!=m-1:
            f.append(',')
        f.append('\n')
    f.append('\t]\n')
    f.append('}')
    return ''.join(f)

